# Documentation

MicroCity Web is **An Online Spatial Planning Tool** reassembled from the desktop version of MicroCity (https://microcity.github.io).

## Table of Contents
- 1 Introduction
  - [1.1 What MicroCity Web can Do](1.1_what_microcity_web_can_do.md)
- 2 Getting Started
  - [2.1 Showing a Rolling Cube](2.1_showing_a_rolling_cube.md)
  - [2.2 Searching for Countries](2.2_searching_for_countries.md)
- 3 User Interfaces
- 4 Scripting Interfaces

## Index
